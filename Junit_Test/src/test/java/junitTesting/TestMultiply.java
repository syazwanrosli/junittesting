package junitTesting;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class TestMultiply {
	
	@Test
	public void testMulti() {
		
		Multiply mu = new Multiply();
		// int output = mu.Multi(3,3); // expecting result in multiply
		// int result = 9; // expected answer
		// assertEquals(result, output);
		assertEquals(9, mu.Multi(3, 3)); // simple one
	}
}
