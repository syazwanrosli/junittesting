package junitTesting;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MultiplicationTest {
	
	@Test
	void multi() {
		Multiplication m = new Multiplication();
		assertEquals(25,m.multiply(5,5));
		System.out.println(25);
	}
	@Test
	void test1() {
		System.out.println("test2");
			}
	@BeforeEach
	void avg() {
		Multiplication avg = new Multiplication();
		assertEquals(6.333333333333333,avg.average(3, 9, 7));
		//System.out.println(6.33);
			System.out.println("Before Each");
		
	}
	@AfterEach
	void add() {
		Multiplication a = new Multiplication();
		assertEquals(10, a.add(5, 5));
		//System.out.println(10);
		System.out.println("After Each");

	}
	@BeforeAll
	static void before() {
		System.out.println("Run before all tests");
	}
	@AfterAll
	static void after() {
		System.out.println("Run After all tests");
	}
}
